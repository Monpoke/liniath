<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {

        $this->addFlash(
            'notice',
            'Your changes were saved!'
        );


        return $this->render('SiteBundle:Default:index.html.twig');
    }
}
